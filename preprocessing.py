import sys
from scipy.signal import butter, lfilter
from scipy.io import loadmat, savemat
from glob import iglob
from os import mkdir, path, listdir, makedirs
import numpy as np
from PIL import Image

MAX = 1
MIN = -1

# region Data pre-processing and saving

# Load the files from the source directory.
# @param source_dir : Relative path to the source directory of the data.
# @return : an iterator for all the files
def load_files(source_dir):
    print('loading files')
    return ((loadmat(file), str(file)) for file in iglob(
        path.join(source_dir, '*.mat')))

# Load the files from the raw directory and save them in a preprocessed way (not filtered)
# @param source_dir : Relative path to the source directory of the data.
# @params target_dir :  the directory where the data should be saved

def extract_to_trial(source_dir, target_dir, is_power_line_removed = False):
    print(source_dir, target_dir, is_power_line_removed)
    input('extract_to_trial called')
    for fname in listdir(source_dir):
        source = path.join(source_dir, fname)
        if path.isdir(source):
            for file_content, filename in load_files(source):
                print('Current file:', filename)
                # The data
                data = file_content['data']
                # Indices where the gesture is made
                indices = np.nonzero(file_content['gesture'])[1]
                # The current gesture
                gesture = file_content['gesture'][0,indices[0]]
                current = indices[0]
                start = indices[0]
                trial = 0
                for index in indices:
                    prec = current
                    current = index
                    if current-prec > 1:
                        end = prec
                        trial += 1
                        x = data[start:end,:]
                        nb_frame = len(x)
                        lower_index = int(-500+nb_frame/2)
                        higher_index = int(500+nb_frame/2)
                        x = x[lower_index:higher_index,:]
                        if is_power_line_removed:
                            x = np.apply_along_axis(bandstop_butterworth, 1, np.abs(x.transpose()), 1000, 2, 45, 55)
                            x = x.transpose()
                        info_dict = {'data': x, 'gesture': gesture, 'subject': file_content['subject'][0][0],
                                     'trial': trial}

                        base = path.basename(filename)
                        file_name = path.splitext(base)[0]+'-'+str(trial).zfill(3)
                        dest_name = path.join(target_dir,fname)
                        if not path.exists(dest_name):
                            makedirs(dest_name)
                        dest_name = path.join(dest_name,file_name)
                        savemat(dest_name,info_dict)



# Save the files are RGB images
# @params target_dir :  the directory where the data should be saved
# @params filename : the filename of the raw data file
# @params data : the preprocessed data
# @params gesture : the gestures for the data
# @params output_type : the type of file to generate
def save_file_rgb(target_dir, filename, data, gesture, output_type='mat'):
    print('saving files')
    if not path.exists(target_dir):
        makedirs(target_dir)
    new_file = path.split(filename)[1]
    new_file = path.splitext(new_file)[0]
    #Normalize the data between 0-1
    data = (data-MIN)/(MAX-MIN)
    # duplicate the data to create the 3 RGB channels
    data = data.reshape((16, 8, data.shape[2], 1))
    rgb_image = np.repeat(data, 3, axis=3)
    if output_type == 'mat':
        savemat(path.join(target_dir, '%s-pp.mat' % new_file), {'data': rgb_image, 'gesture': gesture})
    elif output_type == 'jpeg':
        # rescale the array from 0-255
        # TODO: this don't work, fix it if there is time and need
        rgb_image *= (255 / np.max(rgb_image))
        img = Image.fromarray(rgb_image.astype(np.uint8))
        img.save(path.join(target_dir, '%s-pp.jpeg' % new_file))
    else:
        raise NotImplementedError('This the "%s" format is not supported' % output_type)


# Power line interference removal pre-processing method
# @params source_dir : Relative path to source directory of data
# @params target_dir : the directory where the data should be saved
# @params Fs         : Sampling frequency of the analysed signals
def power_line_inter_removal(source_dir, target_dir, Fs, out_type):
    print('Power line preprocessing\n')
    for file_content, filename in load_files(source_dir):
        print('Current file:', filename)
        # apply filter to data
        gesture = file_content['gesture']
        data = np.apply_along_axis(bandstop_butterworth, 1, np.abs(file_content['data'].transpose()), Fs, 2, 45, 55)
        data.resize(16, 8, data.shape[1])

        # Format the files for PyTorch Compatibility
        # save the files in the target dir and keep the names
        save_file_rgb(target_dir, filename, data, gesture, out_type)
    print('Power line preprocessing done')

# Low pass filtering pre-processing method
# @params source_dir : path the the source directory of the data
# @params target_dir : path where the preprocessed data should be saved
# @params Fs         : Sampling frequency of the analysed signals
def low_pass_filtered(source_dir, target_dir, Fs, out_type):
    print('lowpass filter preprocessing\n')
    for file_content, filename in load_files(source_dir): 
        print('current file:', filename)
        # apply filter to data
        gesture = file_content['gesture']
        data = np.apply_along_axis(lowpass_butterworth, 1, np.abs(file_content['data'].transpose()), Fs, 2, 75)
        data.resize(16, 8, data.shape[1])
        # save the file as RGB
        save_file_rgb(target_dir, filename, data, gesture, out_type)
    print('lowpass filter preprocessing done')

# Extract data to images
# @params source_dir : path the the source directory of the data
# @params target_dir : path where the preprocessed data should be saved
# @params out_type   : the type of file to be generated
def data_to_image(source_dir, target_dir, out_type):
    print('Data to image preprocessing\n')
    for file_content, filename in load_files(source_dir): 
        print('current file:', filename)
        # apply filter to data
        gesture = file_content['gesture']
        data = np.array(file_content['data'])
        data.resize(16, 8, data.shape[1])
        # save the file as RGB
        save_file_rgb(target_dir, filename, data, gesture, out_type)
    print('lowpass filter preprocessing done')


# endregion

# region Filters
# Filter used for data pre-processing.


# Butterworth bandpass filtering
# @params signal : Signal to be filtered.  Needs to be a regularly sampled list or numpy array.
# @params Fs     : Sampling frequency of the signal.
# @params order  : Order of the butterworth filter
# @params f_low  : Low cutoff frequency [Hz]
# @params f_high : High cutoff frequency [Hz]
# @return f_sig  : Filtered signal
def bandpass_butterworth(signal, Fs, order, f_low, f_high):
    nyq = 0.5 * Fs
    coeffs = butter(order, (f_low/nyq, f_high/nyq), btype='bandpass')
    return lfilter(coeffs[0], coeffs[1], signal)


# Butterworth bandstop filtering
# @params signal : Signal to be filtered.  Needs to be a regularly sampled list or numpy array.
# @params Fs     : Sampling frequency of the signal.
# @params order  : Order of the butterworth filter
# @params f_low  : Low cutoff frequency [Hz]
# @params f_high : High cutoff frequency [Hz]
# @return f_sig  : Filtered signal
def bandstop_butterworth(signal, Fs, order, f_low, f_high):
    nyq = 0.5 * Fs
    coeffs = butter(order, (f_low/nyq, f_high/nyq), btype='bandstop')
    return lfilter(coeffs[0], coeffs[1], signal)


# Butterworth lowpass filtering
# @params signal : Signal to be filtered.  Needs to be a regularly sampled list or numpy array.
# @params Fs     : Sampling frequency of the signal.
# @params order  : Order of the butterworth filter
# @params f_c    : Filter cutoff frequency [Hz]
# @return f_sig  : Filtered signal
def lowpass_butterworth(signal, Fs, order, f_c):
    nyq = 0.5 * Fs
    coeffs = butter(order, f_c/nyq)
    return lfilter(coeffs[0], coeffs[1], signal)
# endregion


if __name__ == '__main__':
    """Generate the preprocessed data.   
    
    Usage:
        python preprocessing.py [source directory] [target directory]
    
    Args:
        source_dir (str): The relative path to the directory where 
            the data to be preprocessed is saved.
        target_dir (str): The relative path to where the preprocessed
            data should be stored.
        sampling_frequency (int): The sampling frequency used to 
            collect the data.
    """
    # create help string
    help_string = """
    Usage:
        python preprocessing.py [filter function] [source directory] [target directory] [sampling frequency] [output file type]
        
        filter function can be --lowpass, --power-line, --extract_to_trial or --data-to-image
        if --extract_to_trial is used, the 4 argument is whether or not the power line is removed
        output file type must be either mat (default) or jpeg"""
    # retrieve args
    assert len(sys.argv) > 2, 'Missing arguments\n' + help_string
    # first arg is the filter to apply
    filter_function = sys.argv[1]
    source_dir = sys.argv[2]
    target_dir = sys.argv[3]
    if filter_function == '--extract_to_trial':
        is_power_line_removed = sys.argv[4].lower() == 'true'
    elif filter_function == '--data-to-image':
        out_type = sys.argv[4]
    else:
        sampling_freq = float(sys.argv[4])
    try:
        out_type = sys.argv[5]
    except IndexError:
        out_type = 'mat'
    if filter_function == '--lowpass':
        low_pass_filtered(source_dir, target_dir, sampling_freq, out_type)
    elif filter_function == '--power-line':
        power_line_inter_removal(source_dir, target_dir, sampling_freq, out_type)
    elif filter_function == '--data-to-image':
        data_to_image(source_dir, target_dir, out_type)
    elif filter_function == '--extract_to_trial':
        # sampling_freq arg is is_power_line_removed arg
        extract_to_trial(source_dir, target_dir, is_power_line_removed)
