echo "Downloading dataset from http://zju-capg.org/myo/data/index.html#download"

function download_and_unzip() {
    echo $1
    db=$1
    n_subjects=$2
    # for state in "preprocessed" "raw"; do for subject_id in $(seq -f "%03g" $n_subjects); do
    for state in "preprocessed"; do for subject_id in $(seq -f "%03g" $n_subjects); do
        source_url="http://zju-capg.org/myo/data/$db-$state-$subject_id.zip"
        subject_dir="./$db/$state/$subject_id"

        if [ ! -d $subject_dir ]; then
            mkdir --parents $subject_dir
            wget -q --show-progress $source_url;
            unzip -o $db-$state-$subject_id.zip -d $subject_dir
            rm $db-$state-$subject_id.zip
        fi
    done;done
}

download_and_unzip "dba" 18
download_and_unzip "dbb" 20
download_and_unzip "dbc" 10
