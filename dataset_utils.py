import os
from torch.utils.data import Dataset, DataLoader
import torchvision
import scipy.io
import imageio
import numpy as np
import matplotlib.pyplot as plt
import itertools

DATA_ROOT = "data/"
DB_NAMES = ['dba', 'dbb']
NB_GESTURES = 8


class DataTrial(object):
    '''
    Trial is a frame of 1000 DataSamples of 16x8
    '''

    def __init__(self, path: str, db_name: str, db_type: str, subject: str, pattern: str, trial=None):
        self.path = path
        self.db_name = db_name  # ex: 'dba'
        self.db_type = db_type  # ex: 'preprocessed'
        self.subject = int(subject)
        self.pattern = int(pattern)  # this is the gesture (target)
        self.target = self.pattern - 1
        self.trial = trial

        self.trial_data = scipy.io.loadmat(self.path)['data'].astype(np.float32)
        if self.db_type != 'preprocessed':  # DIRTY
            self.trial_data = np.squeeze(self.trial_data[:,:,:,0])  # remove RGB
        self.samples = [DataSample(self, index) for index in range(1000)]

    def __gt__(self, other):
        return self.path > other.path

    def data(self, index: int) -> np.array:
        # index in range [0:999]
        # TODO: check if more metadata is in loaded file
        # DIRTY
        if self.db_type == 'preprocessed':
            data = np.reshape(self.trial_data[index, :], (16,8))
        else:
            data = self.trial_data[:,:,index]
        return np.array([data])


class DataSample(object):
    '''
    Sample is 16x8 matrix (stored as column in a DataTrial
    '''

    def __init__(self, parent_trial: DataTrial, index: int):
        self.parent_trial = parent_trial
        self.index = index
        self.target = self.parent_trial.target
        self.subject = self.parent_trial.subject
        self.db_name = self.parent_trial.db_name

    @property
    def data(self) -> np.array:
        # index in range [0:999]
        # TODO: check if more metadata is in loaded file
        return self.parent_trial.data(self.index)

    # @property
    # def target(self) -> np.array:
    #     t = np.zeros(NB_GESTURES)
    #     t[self.parent_trial.target] = 1

    def __gt__(self, other):
        if self.parent_trial is other.parent_trial:
            return self.index > other.index
        else:
            return self.parent_trial.path > other.parent_trial.path


class DbPatternAllSubjectsMixed(Dataset):
    """
    Classes are patterns.
    Subject id do not matter.
    """

    def __init__(self, trial_filter=None, db_names=['dba', 'dbb', 'dbc'], data_root=DATA_ROOT, db_type='preprocessed'):
        assert trial_filter in (None, 'keep_odd_only', 'keep_even_only')
        self.trial_filter = trial_filter  # one of
        self.data_root = data_root
        self.db_names = db_names
        self.db_type = db_type

        self.dataset_items = list()
        for db_name in db_names:
            new_data = self.parse_folder_dataset(db_name)
            self.dataset_items.extend(new_data)
        self.dataset_items.sort()

    def __len__(self):
        return len(self.dataset_items)

    def __getitem__(self, index):
        d = self.dataset_items[index]
        return d.data, d.target

    def get_full_item(self, index):
        d = self.dataset_items[index]
        return d.data, d.target, d

    def parse_folder_dataset(self, db_name) -> [str]:
        db_dir = os.path.join(self.data_root, db_name, self.db_type)
        subject_dirs = (f for f in os.listdir(db_dir) if not f.startswith('.'))
        subject_dir_paths = (os.path.join(db_dir, f) for f in subject_dirs if not f.startswith('.'))
        subject_files_lists = (os.listdir(f) for f in subject_dir_paths if os.path.isdir(f))
        subject_files = itertools.chain.from_iterable(subject_files_lists)
        new_dataset_items = (self.parse_data_filename_into_item(db_name, f)
                             for f in subject_files if not f.startswith('.') and self.include_trial(f))
        new_dataset_items = (d for trial in new_dataset_items for d in trial.samples)  # dirty
        return new_dataset_items

    def parse_data_filename_into_item(self, db_name, filename) -> DataTrial:
        parsed = filename[:-4].split('-')[0:3]  # (subject, pattern, trial)
        subject = parsed[0]
        path = os.path.join(self.data_root, db_name, self.db_type, subject, filename)
        d = DataTrial(path, db_name, self.db_type, *parsed)
        return d

    def include_trial(self, filename):
        _, gesture, trial = filename[:-4].split('-')[0:3]  # (subject, pattern, trial)
        if int(gesture) > NB_GESTURES:
            return False
        if self.trial_filter is None:
            return True
        elif (self.trial_filter=='keep_even_only') and (int(trial) % 2 == 0):
            return True
        elif (self.trial_filter=='keep_odd_only') and (int(trial) % 2 == 1):
            return True
        else:
            return False

    def print_dataset_info(self):
        for db in self.db_names:
            c = sum(1 for d in self.dataset_items if d.db_name == db)
            print('{} has {} data items'.format(db, c))

            sub_dataset = [d for d in self.dataset_items if d.db_name == db]
            for s in set(d.subject for d in sub_dataset):
                c = sum(1 for d in sub_dataset if d.subject == s)
                print('\tsubject_id: {} has {} data items'.format(s, c))


if __name__ == "__main__":
    # dataset_all = DbPatternAllSubjectsMixed(db_names=['dba', 'dbb', 'dbc'])
    dataset_all = DbPatternAllSubjectsMixed(db_names=['dba'])
    dataset_all.print_dataset_info()

    # dirty display tests
    display_gesture_id = 1

    target_idx = [i for i in range(len(dataset_all)) if dataset_all[i][1] == display_gesture_id]

    # target_idx = target_idx[:10]  # displaying all is way too big
    # fig, axes = plt.subplots(nrows=1, ncols=len(target_idx))
    # plt.title('Gesture: {}'.format(display_gesture_id))
    # for i, y in enumerate(target_idx):
    #     img, target, data_item = dataset_all[y]
    #     axes[i].imshow(img)
    # plt.show()

    nb_img = len(target_idx)
    big_ass_img = np.zeros((1000, nb_img * 129), dtype=float)
    plt.title('Gesture: {}'.format(display_gesture_id))

    for i, y in enumerate(target_idx):
        img, target, data_item = dataset_all.get_full_item(y)
        big_ass_img[:, i * 129:(i + 1) * 129 - 1] = img.astype(float)
    # imageio.imsave('gesture001.png', big_ass_img)
    # plt.imshow(big_ass_img)
    plt.tight_layout()
    plt.show()
