###############################################################################
# Project : Reproducing the ConvNet of : http://zju-capg.org/myo/index.html
# GIF-4101 / GIF-7005, Fall 2018
#
import sys
import time

import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim import SGD
import torchvision
from torch.utils.data.sampler import SubsetRandomSampler
from torch.utils.data import Dataset, DataLoader
from utils import compute_accuracy, compute_confusion_matrix
from dataset_utils import DbPatternAllSubjectsMixed

from torch.utils import data

from dataset_utils import NB_GESTURES
from dataset_utils import DB_NAMES

BATCH_SIZE = 1000


class LocallyConnected2d_1x1(nn.Module):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.shape = (14, 6)
        self.layers = []
        for i in range(self.shape[0] * self.shape[1]):
            self.layers.append(nn.Conv1d(64, 64, 1).to('cuda'))

    def forward(self, y):
        y = y.view(BATCH_SIZE, 64, -1)
        for i, layer in enumerate(self.layers):
            y[:, :, i, np.newaxis] = layer(y[:, :, i, np.newaxis])
        return y.view(BATCH_SIZE, 64, -1, 1)


class ConvNet(nn.Module):
    def __init__(self):
        super().__init__()

        # Convolution layers
        # (input_size, output_size, kernel_size
        # TODO check padding
        self.C1 = nn.Conv2d(1, 64, 3, padding=0)
        self.C2 = nn.Conv2d(64, 64, 3, padding=1)

        # Locally connected layers
        # can be replaced with the following conv2d for better results
        # self.LC3 = nn.Conv2d(64, 64, 1)
        # self.LC4 = nn.Conv2d(64, 64, 1)
        self.LC3 = LocallyConnected2d_1x1(64, 64, 1)
        self.LC4 = LocallyConnected2d_1x1(64, 64, 1)

        # Fully connected layes
        self.FC5 = nn.Linear(64 * 14 * 6, 512)
        self.FC6 = nn.Linear(512, 512)
        self.FC7 = nn.Linear(512, 128)

        # Batch normalization
        self.B0 = nn.BatchNorm2d(1)
        self.B1 = nn.BatchNorm2d(64)
        self.B2 = nn.BatchNorm2d(64)
        self.B3 = nn.BatchNorm2d(64)
        self.B4 = nn.BatchNorm2d(64)
        self.B5 = nn.BatchNorm2d(64)
        self.B6 = nn.BatchNorm2d(512)
        self.B7 = nn.BatchNorm2d(128)

        # Dropout layers
        self.DO4 = nn.Dropout(0.5)
        self.DO5 = nn.Dropout(0.5)
        self.DO6 = nn.Dropout(0.5)

        # Output layer
        self.fc8 = nn.Linear(128, NB_GESTURES)
        self.sm0 = nn.Softmax(dim=1)

    def init_locally_connected_1x1(self, shape):
        x, y = shape

        # for i in range(x):
        #     for j in range(y):
        #         arr_idx =
        torch.resize()

    def hidden_forward(self, x):
        # Hidden layers inference
        x = x.float()
        x = self.B0(x)
        y1 = F.relu(self.B1(self.C1(x)))
        y2 = F.relu(self.B2(self.C2(y1)))
        y3 = F.relu(self.B3(self.LC3(y2)))
        y4 = F.relu(self.B4(self.DO4(self.LC4(y3))))
        y4 = y4.view(BATCH_SIZE, -1)

        # y5 = F.relu(self.B5(self.DO5(self.FC5(y4))))
        # y6 = F.relu(self.B6(self.DO6(self.FC6(y5))))
        # y = F.relu(self.B7(self.FC7(y6)))
        y5 = F.relu(self.DO5(self.FC5(y4)))
        y6 = F.relu(self.DO6(self.FC6(y5)))
        y = F.relu(self.FC7(y6))
        return y

    def forward(self, x):
        # Input size
        batch_size = x.size()[0]

        # Execute hidden layers
        y = self.hidden_forward(x)

        # return torch.nn.Softmax(self.fc8(y))
        return self.sm0(self.fc8(y))


# def split_dataset_into_train_test(test_proportion: float, shuffle=True, seed=42) -> (DataLoader, DataLoader):
#     dataset = DbPatternAllSubjectsMixed(db_names=['dba', 'dbb', 'dbc'])
#     all_idx = np.arange(0, len(dataset))
#     split = np.floor(test_proportion*len(dataset)).astype(int)
#     if shuffle:
#         np.random.seed(seed)
#         np.random.shuffle(all_idx)
#     train_indices, val_indices = all_idx[split:], all_idx[:split]
#
#     train_loader = DataLoader(dataset, batch_size=batch_size,sampler=SubsetRandomSampler(train_indices))
#     test_loader = DataLoader(dataset, batch_size=batch_size, sampler=SubsetRandomSampler(val_indices))
#
#     return train_loader, test_loader


if __name__ == '__main__':
    model_name = 'LC_relu_%s_DB_%s_' % (BATCH_SIZE, '-'.join(DB_NAMES))
    if len(sys.argv) > 1:
        db_type_index = sys.argv[1]
    else:
        db_type_index = 4

    # db_types = ['preprocessed',  # original paper's
    #             'processed', 'processed-LowPass', 'processed-LowPassMAXNORM',  # ours
    #             'processedPL', 'processedPL-LowPass', 'processedPL-LowPassMAXNORM', ]
    db_types = ['preprocessed',  # original paper's
                'dba-processedPL', 'dba-processed-LowPass', 'dba-processed', 'dba-processedPL-LowPass',
                'processed', 'processed-LowPass', 'processed-LowPassMAXNORM',  # ours
                'processedPL', 'processedPL-LowPass', 'processedPL-LowPassMAXNORM', ]
    db_type = db_types[int(db_type_index)]
    print("DB_TYPE: %s" % db_type)
    RANDOM_SEED = 42
    torch.manual_seed(RANDOM_SEED)
    torch.cuda.manual_seed_all(RANDOM_SEED)

    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    print('Using %s' % device)

    # Training parameters
    nb_epoch = 28
    learning_rate = 0.1  # Div by 10 after 16th & 24th epochs
    momentum = 0.9  # Not specified in paper
    batch_size = BATCH_SIZE

    # train_loader, test_loader = split_dataset_into_train_test(0.5)

    train_dataset = DbPatternAllSubjectsMixed(trial_filter='keep_odd_only', db_names=DB_NAMES, db_type=db_type)
    test_dataset = DbPatternAllSubjectsMixed(trial_filter='keep_even_only', db_names=DB_NAMES, db_type=db_type)

    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=True)

    # Instantiate network model
    model = ConvNet()
    # model.load_state_dict(torch.load('ConvNet_model_1545374359.331546.pt'))
    model.to(device)

    # Instantiate error function
    criterion = nn.MultiLabelSoftMarginLoss()

    # Instantiate optimizer
    optimizer = SGD(model.parameters(), lr=learning_rate, momentum=momentum)

    # Network training mode
    model.train()

    test_accuracy_list = []
    train_accuracy_list = []
    # test_accuracy_list.append(compute_accuracy(model, test_loader, device=device))
    # train_accuracy_list.append(compute_accuracy(model, train_loader, device=device))
    # print('test_accuracy: %s' % test_accuracy_list)
    # print('train_accuracy: %s' % train_accuracy_list)
    # import sys
    # sys.exit()

    # Training loop
    for i_epoch in range(nb_epoch):
        if i_epoch == 16 or i_epoch == 24:
            learning_rate /= 10
            optimizer = SGD(model.parameters(), lr=learning_rate, momentum=momentum)  # Update optimizer with new lr
        start_time, train_losses = time.time(), []
        for i_batch, batch in enumerate(train_loader):
            images, targets = batch

            images = images.to(device)
            targets = targets.to(device)

            # Initialize gradients to 0
            optimizer.zero_grad()

            # one hot vector cheat
            target_arrays = torch.eye(NB_GESTURES)[targets].to(device)

            # Run the inference and evaluate loss
            predictions = model.forward(images)

            loss = criterion(predictions, target_arrays)

            # Backpropagation of the error + run optimizer step
            loss.backward()
            optimizer.step()

            # Add loss of current batch
            train_losses.append(loss.item())

        print(' [-] epoch {:4}/{:}, train loss {:.6f} in {:.2f}s'.format(
            i_epoch + 1, nb_epoch, np.mean(train_losses), time.time() - start_time))

        # save model
        torch.save(model.state_dict(), 'ConvNet_model_db-%s_epoch-%s_timestamp-%s.pt' % (db_type, i_epoch, time.time()))
        test_accuracy_list.append(compute_accuracy(model, test_loader, device=device))
        train_accuracy_list.append(compute_accuracy(model, train_loader, device=device))
        print('test_accuracy: %s' % test_accuracy_list)
        print('train_accuracy: %s' % train_accuracy_list)
        np.savetxt('test-accuracy_db-%s_model-%s.txt' % (db_type, model_name), test_accuracy_list)
        np.savetxt('train-accuracy_db-%s_model-%s.txt' % (db_type, model_name), train_accuracy_list)

    # Score display
    test_acc = compute_accuracy(model, test_loader, device)
    print(' [-] test acc. {:.6f}%'.format(test_acc * 100))

    # Confusion matrix display
    matrix = compute_confusion_matrix(model, test_loader, device)
    print(' [-] conf. mtx. : \n{}'.format(matrix))
