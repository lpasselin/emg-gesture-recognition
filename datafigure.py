import matplotlib.pyplot as plt
import numpy as np
from scipy.io import loadmat, savemat

def visualizeSignal():
    raw_data = loadmat('data/raw.mat')
    raw_data = raw_data['data']

    extracted_data = loadmat('data/extracted.mat')
    extracted_data = extracted_data['data']

    plt.figure()
    plt.subplot(2, 1, 1)
    plt.plot(raw_data[:,0])
    plt.xlabel('Échantillons')
    plt.ylabel('Amplitude')
    plt.title('Signal brut pour le geste 1 sur le premier canal')
    plt.grid()
    plt.ylim([-1,1])
    plt.xlim(left=0)

    plt.subplot(2, 1, 2)
    plt.plot(extracted_data[:,0])
    plt.xlabel('Échantillons')
    plt.ylabel('Amplitude')
    plt.title("Premier signal d'intérêt du geste 1 sur le premier canal")
    plt.grid()
    plt.ylim([-0.501,0.501])
    plt.xlim([0,1000])


    plt.tight_layout()
    plt.show()
visualizeSignal()