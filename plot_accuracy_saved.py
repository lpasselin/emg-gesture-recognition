import matplotlib.pyplot as plt
import numpy as np
import os


# def plot_accuracy_list(name_suffix):
#     test_name = 'test-accuracy' + name_suffix
#     train_name = 'train-accuracy' + name_suffix
#     test_accuracy_list = np.loadtxt(test_name)
#     train_accuracy_list = np.loadtxt(train_name)
#
#     if not ' ' in name_suffix:
#         return
#
#     try:
#         print(len(test_accuracy_list))
#         if len(test_accuracy_list) < 3:
#             return
#     except:
#         return
#
#     test_name = test_name.replace('.txt', '')
#     train_name = train_name.replace('.txt', '')
#
#     # if abs(test_accuracy_list[-1] - train_accuracy_list[-1]) > 0.3:
#     #     return
#
#     plt.plot(test_accuracy_list, label=test_name, marker='o')
#     plt.plot(train_accuracy_list, label=train_name)

def plot_accuracy_list(name_suffix):
    test_name = 'test-accuracy' + name_suffix
    train_name = 'train-accuracy' + name_suffix
    test_accuracy_list = np.loadtxt(test_name)
    train_accuracy_list = np.loadtxt(train_name)

    if ' ' in name_suffix or '2' in name_suffix or 'Low' in name_suffix or 'db-processed' in name_suffix:
        return

    try:
        print(len(test_accuracy_list))
        if len(test_accuracy_list) < 15:
            return
    except:
        return

    test_name = test_name.replace('.txt', '')
    train_name = train_name.replace('.txt', '')

    # if abs(test_accuracy_list[-1] - train_accuracy_list[-1]) > 0.3:
    #     return

    plt.plot(test_accuracy_list, label=test_name, marker='o')
    plt.plot(train_accuracy_list, label=train_name)


accuracy_filenames = []
for i in os.listdir('.'):

    if i.startswith('test-accuracy'):
        accuracy_filenames.append(i.replace('test-accuracy', ''))

plt.figure()
for f in accuracy_filenames:
    plot_accuracy_list(f)

plt.xlabel('Epoch')
plt.ylabel('Accuracy')
# plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.01))
plt.legend()
plt.show()
