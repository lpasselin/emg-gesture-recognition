###############################################################################
# Project utils taken from:
# Apprentissage et reconnaissance
# GIF-4101 / GIF-7005, Automne 2018
# Devoir 4, Code utilitaire
#
###############################################################################

import gzip
import random

import numpy as np

from sklearn.metrics import confusion_matrix

import torch
import torch.nn as nn

def compute_accuracy(model, dataloader, device='cpu'):
    training_before = model.training
    model.eval()

    all_predictions = []
    all_targets = []
    for i_batch, batch in enumerate(dataloader):
        # if i_batch > 32:
        #     print('WATCH OUT DIRTY RETURN FROM COMPUTE_ACCURACY')
        #     break
        images, targets = batch

        images = images.to(device)
        targets = targets.to(device)

        with torch.no_grad():
            predictions = model(images)

        all_predictions.append(predictions.cpu().numpy())
        all_targets.append(targets.cpu().numpy())

    if all_predictions[0].shape[-1] > 1:
        predictions_numpy = np.concatenate(all_predictions, axis=0)
        predictions_numpy = predictions_numpy.argmax(axis=1)
        targets_numpy = np.concatenate(all_targets, axis=0)
    else:
        predictions_numpy = np.ravel(all_predictions)
        targets_numpy = np.ravel(all_targets)
        predictions_numpy[predictions_numpy >= 0.5] = 1.0
        predictions_numpy[predictions_numpy < 0.5] = 0.0

    if training_before:
        model.train()


    return (predictions_numpy == targets_numpy).mean()


def compute_confusion_matrix(model, dataloader, device='cpu'):
    training_before = model.training
    model.eval()

    all_predictions = []
    all_targets = []
    dataloader.balanced = False
    for i_batch, batch in enumerate(dataloader):
        images, targets = batch

        images = images.to(device)
        targets = targets.to(device)

        with torch.no_grad():
            predictions = model(images)

        all_predictions.append(predictions.cpu().numpy())
        all_targets.append(targets.cpu().numpy())
    dataloader.balanced = True

    predictions_numpy = np.ravel(all_predictions)
    targets_numpy = np.ravel(all_targets)

    predictions_numpy[predictions_numpy >= 0.5] = 1.0
    predictions_numpy[predictions_numpy < 0.5] = 0.0

    if training_before:
        model.train()

    matrix = confusion_matrix(targets_numpy, predictions_numpy)
    matrix = matrix / matrix.sum(axis=1)[:, np.newaxis]

    return matrix