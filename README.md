# emg-gesture-recognition

pytorch model based on [Gesture Recognition by Instantaneous Surface EMG Images](http://zju-capg.org/myo/index.html)


#### download dataset

cd data && ./download.sh

Downloads the [CapgMyo](http://zju-capg.org/myo/data/index.html#download) database